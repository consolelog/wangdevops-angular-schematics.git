import { SchematicTestRunner, UnitTestTree } from '@angular-devkit/schematics/testing';
import {
  createTestRunner,
  createTestTree,
  defaultAppComponentContent,
  defaultSharedModule,
  newAppModule,
} from '../utils/testing';


describe('ng-add', () => {

  let appTree: UnitTestTree;
  let testRunner: SchematicTestRunner;

  beforeEach(async () => {
    testRunner = createTestRunner();
    appTree = await createTestTree(testRunner);
  });

  it('判断是否初始化成功', async () => {
    expect(appTree.files).toContain('/angular.json');
    expect(appTree.files).toContain('/package.json');
    expect(appTree.files).toContain('/README.md');
    expect(appTree.files).toContain('/tsconfig.json');
    expect(appTree.files).toContain('/.editorconfig');
    expect(appTree.files).toContain('/.gitignore');
    expect(appTree.files).toContain('/.vscode/extensions.json');
    expect(appTree.files).toContain('/.vscode/launch.json');
    expect(appTree.files).toContain('/.vscode/tasks.json');
    expect(appTree.files).toContain('/projects/bar/tsconfig.app.json');
    expect(appTree.files).toContain('/projects/bar/tsconfig.spec.json');
    expect(appTree.files).toContain('/projects/bar/src/favicon.ico');
    expect(appTree.files).toContain('/projects/bar/src/index.html');
    expect(appTree.files).toContain('/projects/bar/src/main.ts');
    expect(appTree.files).toContain('/projects/bar/src/styles.css');
    expect(appTree.files).toContain('/projects/bar/src/assets/.gitkeep');
    expect(appTree.files).toContain('/projects/bar/src/app/app.module.ts');
    expect(appTree.files).toContain('/projects/bar/src/app/app.component.html');
    expect(appTree.files).toContain('/projects/bar/src/app/app.component.spec.ts');
    expect(appTree.files).toContain('/projects/bar/src/app/app.component.ts');
    expect(appTree.files).toContain('/projects/bar/src/app/app.component.css');
    let actual: any = appTree.readContent('/projects/bar/src/app/app.component.ts');
    expect(actual.replaceAll('\r\n', '\n')).toContain(defaultAppComponentContent);
  });

  it('测试ng-add是否执行正确', async () => {
    let tree = await testRunner.runSchematicAsync('ng-add', {}, appTree).toPromise();
    let files = tree.files;
    expect(files).toContain('/package.json');
    let sharedModulePath = '/projects/bar/src/app/shared/shared.module.ts';
    expect(files).toContain(sharedModulePath);
    let actual: any = tree.readContent(sharedModulePath);
    expect(actual.replaceAll('\r\n', '\n')).toBe(defaultSharedModule);
    let content = tree.readContent('/package.json');
    let packageJson = JSON.parse(content);
    expect(packageJson.dependencies['@ngxs/store']).toBeDefined();
    expect(packageJson.scripts.start).toBe('ng serve --proxy-config proxy.json');
    expect(files).toContain('/projects/bar/src/app/store/index.ts');
    expect(files).toContain('/projects/bar/src/app/store/router/custom-router-state-serializer.ts');
    expect(files).toContain('/projects/bar/src/app/store/router/index.ts');
    expect(files).toContain('/projects/bar/src/app/store/router/router.selector.ts');
    expect(files).toContain('/projects/bar/src/app/store/system/index.ts');
    expect(files).toContain('/projects/bar/src/app/store/system/system.action.ts');
    expect(files).toContain('/projects/bar/src/app/store/system/system.selector.ts');
    expect(files).toContain('/projects/bar/src/app/store/system/system.state.ts');
    let actual1: any = tree.readContent('/projects/bar/src/app/app.module.ts');
    expect(actual1.replaceAll('\r\n', '\n')).toBe(newAppModule);
  });

  it('测试standard-version是否正确添加', async () => {
    let tree = await testRunner.runSchematicAsync('ng-add', {}, appTree).toPromise();
    let files = tree.files;
    let content = tree.readContent('/package.json');
    let packageJson = JSON.parse(content);
    // 包含release命令
    expect(packageJson.scripts.release).toBe('standard-version');
    // 包含standard-version依赖
    expect(Object.keys(packageJson.devDependencies)).toContain('standard-version');
    // 包含.versionrc文件
    expect(files).toContain('/projects/bar/.versionrc');
  });

  it('如果ngAdd的时候有文件重复该怎么办', async () => {
    appTree.create('/projects/bar/src/app/shared/shared.module.ts', defaultSharedModule);
    let tree = await testRunner.runSchematicAsync('ng-add', {}, appTree).toPromise();
    let files = tree.files;
    expect(files).toContain('/projects/bar/src/app/shared/shared.module.ts');
  });

  it('测试是否正确添加@angular/cdk', async () => {
    let tree = await testRunner.runSchematicAsync('ng-add', {}, appTree).toPromise();
    let content = tree.readContent('/package.json');
    let packageJson = JSON.parse(content);
    // 包含standard-version依赖
    expect(Object.keys(packageJson.dependencies)).toContain('@angular/cdk');
  });
});
