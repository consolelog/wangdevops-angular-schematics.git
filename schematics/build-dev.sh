set -e
if [ -e dist ]; then
  rm -rf dist
fi
./node_modules/typescript/bin/tsc -p tsconfig.dev.json
rm -rf ./dist/**/*.d.ts
rm -rf ./dist/**/*spec.js
rm -rf ./dist/**/*spec.js.map
cp -r src/ng-add/files dist/src/ng-add
cp -r src/generate-state/files dist/src/generate-state
cp README.md dist/README.md
cp src/collection.json dist/src/collection.json
cp src/ng-add/schema.json dist/src/ng-add/schema.json
cp src/generate-state/schema.json dist/src/generate-state/schema.json
cp src/fix-state/schema.json dist/src/fix-state/schema.json
cp package.json dist/package.json
