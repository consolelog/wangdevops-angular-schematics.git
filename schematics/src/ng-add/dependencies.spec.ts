import { Tree } from '@angular-devkit/schematics';
import { addDependencies } from './dependencies';
import { SchematicTestRunner, UnitTestTree } from '@angular-devkit/schematics/testing';
import { createTestRunner, createTestTree } from '../utils/testing';
import { getFileContent } from '../utils/get-file-content';

describe('dependencies', () => {
  let appTree: UnitTestTree;
  let testRunner: SchematicTestRunner;

  beforeEach(async () => {
    testRunner = createTestRunner();
    appTree = await createTestTree(testRunner);
  });

  it('判断addDependencies方法是否正确的更新package.json文件', async () => {
    let tree: Tree = await testRunner.callRule(addDependencies, appTree).toPromise();
    let content = getFileContent(tree, '/package.json');
    let packageJson = JSON.parse(content);
    expect(packageJson.dependencies['@ngxs/store']).toBeDefined();
    expect(packageJson.dependencies['@ngxs/form-plugin']).toBeDefined();
    expect(packageJson.dependencies['@ngxs/router-plugin']).toBeDefined();
    expect(packageJson.dependencies['@ngxs/storage-plugin']).toBeDefined();
    expect(packageJson.dependencies['lodash']).toBeDefined();
    expect(packageJson.devDependencies['@wangdevops/angular-schematics']).toBeDefined();
    expect(packageJson.devDependencies['@types/lodash']).toBeDefined();
  });
});
