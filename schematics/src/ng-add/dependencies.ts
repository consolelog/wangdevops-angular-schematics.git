import { SchematicContext, Tree } from '@angular-devkit/schematics';
import { NodePackageInstallTask } from '@angular-devkit/schematics/tasks';

const dependencies = [
  {
    name: '@ngxs/store',
    version: '*',
  },
  {
    name: '@ngxs/form-plugin',
    version: '*',
  },
  {
    name: '@ngxs/router-plugin',
    version: '*',
  },
  {
    name: '@ngxs/storage-plugin',
    version: '*',
  },
  {
    name: 'lodash',
    version: '*',
  },
  {
    name: 'object-path-immutable',
    version: '*',
  },
  {
    name: 'ngx-auto-unsubscribe-decorator',
    version: '*',
  },
];
const devDependencies = [
  {
    name: '@wangdevops/angular-schematics',
    version: '*',
  },
  {
    name: '@types/lodash',
    version: '*',
  },
  {
    name: 'standard-version',
    version: '*',
  },
];

export function addDependencies() {
  return (_tree: Tree, _context: SchematicContext) => {
    dependencies.forEach(d => {
      addDependencyToPackageJson(
          _tree,
          'dependencies',
          d.name,
          d.version,
      );
    });
    devDependencies.forEach(d => {
      addDependencyToPackageJson(
          _tree,
          'devDependencies',
          d.name,
          d.version,
      );
    });
    return _tree;
  };
}

export function addPackageInstallTask() {
  return (_tree: Tree, _context: SchematicContext) => {
    const depNames = dependencies.map(d => d.name).join(' ');
    const devDepNames = devDependencies.map(d => d.name).join(' ');
    _context.addTask(
        new NodePackageInstallTask({
          packageName: depNames + ' ' + devDepNames,
        }),
    );
    return _tree;
  };
}

export type DependencyTypes =
    | 'dependencies'
    | 'devDependencies'
    | 'optionalDependencies'
    | 'peerDependencies';

function sortObjectByKeys(obj: any): any {
  return Object.keys(obj)
      .sort()
      .reduce((result: any, key: string) => (result[key] = obj[key]) && result, {});
}

/**
 * Adds a package to the package.json
 */
export function addDependencyToPackageJson(
    tree: Tree,
    type: DependencyTypes,
    pkg: string,
    version: string,
): Tree {
  if (tree.exists('/package.json')) {
    const sourceText = tree.read('/package.json')!.toString('utf-8');
    const json = JSON.parse(sourceText);
    if (!json[type]) {
      json[type] = {};
    }

    if (!json[type][pkg]) {
      json[type][pkg] = version;
      json.dependencies = sortObjectByKeys(json.dependencies);
    }

    tree.overwrite('/package.json', JSON.stringify(json, null, 2));
  }

  return tree;
}
