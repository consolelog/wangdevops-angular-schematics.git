import { SchematicTestRunner, UnitTestTree } from '@angular-devkit/schematics/testing';
import { createTestRunner, createTestTree } from '../utils/testing';

describe('generate-state', () => {
  let appTree: UnitTestTree;
  let testRunner: SchematicTestRunner;

  beforeEach(async () => {
    testRunner = createTestRunner();
    appTree = await createTestTree(testRunner);
  });
  const expectStateFile = `import { Injectable } from '@angular/core';
import { Action, State, StateContext } from '@ngxs/store';
import { UserAction } from '.';

export interface UserStateModel {
  id: number;
}

@State<UserStateModel>({
  name: 'user',
  defaults: {
    id: 1,
  },
})
@Injectable({
  providedIn: 'root',
})
export class UserState {
  @Action(UserAction.ChangeId)
  changeId(ctx: StateContext<UserStateModel>, data: UserAction.ChangeId) {
    ctx.patchState({
      id: data.id,
    });
  }
}
`;
  it('state command works', async () => {
    const options = {name: 'user', path: '/projects/bar/src/app/pages/user'};
    let tree = await testRunner.runSchematic('state', options, appTree);
    expect(tree.files).toContain('/projects/bar/src/app/pages/user/user/user.action.ts');
    expect(tree.files).toContain('/projects/bar/src/app/pages/user/user/user.selector.ts');
    expect(tree.files).toContain('/projects/bar/src/app/pages/user/user/user.state.ts');
    expect(tree.files).toContain('/projects/bar/src/app/pages/user/user/index.ts');
    let actual: any = tree.readContent('/projects/bar/src/app/pages/user/user/user.state.ts');
    expect(actual.replaceAll('\r\n', '\n')).toBe(expectStateFile);
  });

  it('generate-state command works', async () => {
    const options = {name: 'user', path: '/projects/bar/src/app/pages/user'};
    let tree = await testRunner.runSchematic('generate-state', options, appTree);
    let actual: any = tree.readContent('/projects/bar/src/app/pages/user/user/user.state.ts');
    expect(actual.replaceAll('\r\n', '\n')).toBe(expectStateFile);
  });
  const expectStateFileWithDash = `import { Injectable } from '@angular/core';
import { Action, State, StateContext } from '@ngxs/store';
import { UserOrderListAction } from '.';

export interface UserOrderListStateModel {
  id: number;
}

@State<UserOrderListStateModel>({
  name: 'userOrderList',
  defaults: {
    id: 1,
  },
})
@Injectable({
  providedIn: 'root',
})
export class UserOrderListState {
  @Action(UserOrderListAction.ChangeId)
  changeId(ctx: StateContext<UserOrderListStateModel>, data: UserOrderListAction.ChangeId) {
    ctx.patchState({
      id: data.id,
    });
  }
}
`;
  it('state command name with dash', async () => {
    const options = {name: 'user-order-list', path: '/projects/bar/src/app/pages/user'};
    let tree = await testRunner.runSchematic('state', options, appTree);
    expect(tree.files).toContain('/projects/bar/src/app/pages/user/user-order-list/user-order-list.action.ts');
    expect(tree.files).toContain('/projects/bar/src/app/pages/user/user-order-list/user-order-list.selector.ts');
    expect(tree.files).toContain('/projects/bar/src/app/pages/user/user-order-list/user-order-list.state.ts');
    expect(tree.files).toContain('/projects/bar/src/app/pages/user/user-order-list/index.ts');
    let actual: any = tree.readContent('/projects/bar/src/app/pages/user/user-order-list/user-order-list.state.ts');
    expect(actual.replaceAll('\r\n', '\n')).toEqual(expectStateFileWithDash);
  });
});
