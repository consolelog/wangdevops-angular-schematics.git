import { apply, applyTemplates, mergeWith, move, Rule, SchematicContext, Tree, url } from '@angular-devkit/schematics';
import { camelize, classify, dasherize } from '@angular-devkit/core/src/utils/strings';
import { normalize } from '@angular-devkit/core';


// You don't have to export the function as default. You can also have more than one rule factory
// per file.
export function generateState(_options: any): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    // 文件名: test.order.name
    let name0 = _options.name;
    _context.logger.debug(`文件名:\t${name0}`);
    // 大驼峰命名:TestUserOrder
    let name1 = classify(camelize(_options.name));
    _context.logger.debug(`大驼峰命名:\t${name1}`);
    // 小驼峰命名:testUserOrder
    let name2 = camelize(_options.name);
    _context.logger.debug(`小驼峰命名:\t${name2}`);
    // 横线分割:test-user-order
    let name3 = dasherize(name1);
    _context.logger.debug(`横线分割:\t${name3}`);
    return mergeWith(apply(url('./files'), [
      applyTemplates({
        name0, name1, name2, name3,
      }),
      move(normalize(`${_options.path}/${name3}`)),
    ]));
  };
}
