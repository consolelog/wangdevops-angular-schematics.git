import { SchematicTestRunner, UnitTestTree } from '@angular-devkit/schematics/testing';
import { createTestRunner, createTestTree } from '../utils/testing';
import { Tree } from '@angular-devkit/schematics';
import { addImport, addProvider, removeModule } from './setup';
import { getFileContent } from '../utils/get-file-content';

describe('setup', () => {
  let appTree: UnitTestTree;
  let testRunner: SchematicTestRunner;

  beforeEach(async () => {
    testRunner = createTestRunner();
    appTree = await createTestTree(testRunner);
  });

  it('addImport', async () => {
    let appModulePath = '/projects/bar/src/app/app.module.ts';
    let tree: Tree = await testRunner.callRule(addImport, appTree).toPromise();
    const newAppModuleContent = `import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { RouterStateSerializer } from '@ngxs/router-plugin';
import { states } from './store';
import { CustomRouterStateSerializer } from './store';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
`;
    expect(getFileContent(tree, appModulePath)).toBe(newAppModuleContent);
  });

  it('removeModule', async () => {
    const appModulePath = '/projects/bar/src/app/app.module.ts';
    const oldAppModuleContent = `import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
`;
    appTree.overwrite(appModulePath, oldAppModuleContent);
    let tree: Tree = await testRunner.callRule(removeModule, appTree).toPromise();
    const newAppModuleContent = `import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
`;
    expect(getFileContent(tree, appModulePath)).toBe(newAppModuleContent);
  });

  it('addProvider', async () => {
    let appModulePath = '/projects/bar/src/app/app.module.ts';
    let tree: Tree = await testRunner.callRule(addProvider, appTree).toPromise();
    const newAppModuleContent = `import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { RouterStateSerializer } from '@ngxs/router-plugin';
import { CustomRouterStateSerializer } from './store';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    {provide: RouterStateSerializer, useClass: CustomRouterStateSerializer}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
`;
    expect(getFileContent(tree, appModulePath)).toBe(newAppModuleContent);
  });
});
