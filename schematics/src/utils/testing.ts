import { Schema as WorkspaceOptions } from '@schematics/angular/workspace/schema';
import { Schema as ApplicationOptions, Style } from '@schematics/angular/application/schema';
import { SchematicTestRunner, UnitTestTree } from '@angular-devkit/schematics/testing';
import * as path from 'path';
import { getProjectFromWorkspace, getProjectMainFile, parseSourceFile } from '@angular/cdk/schematics';
import { Tree } from '@angular-devkit/schematics';
import { getWorkspace } from '@schematics/angular/utility/workspace';
import { getAppModulePath } from '@schematics/angular/utility/ng-ast-utils';

export const workspaceOptions: WorkspaceOptions = {
  name: 'workspace',
  newProjectRoot: 'projects',
  version: '6.0.0',
};

export const appOptions: ApplicationOptions = {
  name: 'bar',
  inlineStyle: false,
  inlineTemplate: false,
  routing: false,
  style: Style.Css,
  skipTests: false,
  skipPackageJson: false,
};

export function createTestRunner(): SchematicTestRunner {
  const collectionPath = path.join(__dirname, '../collection.json');
  return new SchematicTestRunner('schematics', collectionPath);
}

export async function createTestTree(runner: SchematicTestRunner): Promise<UnitTestTree> {
  let appTree: UnitTestTree;
  appTree = await runner.runExternalSchematic('@Schematics/angular', 'workspace', workspaceOptions);
  appTree = await runner.runExternalSchematic('@Schematics/angular', 'application', appOptions, appTree);
  return appTree;
}

export async function resolveTree(_tree: Tree) {
  const workspace = await getWorkspace(_tree);
  let next = workspace.projects.entries().next();
  const project = getProjectFromWorkspace(workspace, next.value[0]);
  const appModulePath = getAppModulePath(_tree, getProjectMainFile(project));
  const moduleSource = parseSourceFile(_tree, appModulePath);
  return {
    workspace,
    project,
    appModulePath,
    moduleSource,
    sourceRoot: project.sourceRoot,
  };
}

export const defaultAppComponentContent = `import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'bar';
}
`;

export const defaultAppModule = `import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
`;

export const defaultFiles = [
  '/README.md',
  '/.editorconfig',
  '/.gitignore',
  '/angular.json',
  '/package.json',
  '/tsconfig.json',
  '/.vscode/extensions.json',
  '/.vscode/launch.json',
  '/.vscode/tasks.json',
  '/projects/bar/.browserslistrc',
  '/projects/bar/karma.conf.js',
  '/projects/bar/tsconfig.app.json',
  '/projects/bar/tsconfig.spec.json',
  '/projects/bar/src/favicon.ico',
  '/projects/bar/src/index.html',
  '/projects/bar/src/main.ts',
  '/projects/bar/src/polyfills.ts',
  '/projects/bar/src/styles.css',
  '/projects/bar/src/test.ts',
  '/projects/bar/src/assets/.gitkeep',
  '/projects/bar/src/environments/environment.prod.ts',
  '/projects/bar/src/environments/environment.ts',
  '/projects/bar/src/app/app.module.ts',
  '/projects/bar/src/app/app.component.css',
  '/projects/bar/src/app/app.component.html',
  '/projects/bar/src/app/app.component.spec.ts',
  '/projects/bar/src/app/app.component.ts',
];

export const defaultSharedModule = `import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxsFormPluginModule } from '@ngxs/form-plugin';

const THIRD_MODULES: any[] = [
  FormsModule,
  ReactiveFormsModule,
  HttpClientModule,
  NgxsFormPluginModule,
];
const NG_ZORRO_MODULES: any[] = [];
const COMPONENTS: any[] = [];
const DIRECTIVES: any[] = [];
const PIPES: any[] = [];

@NgModule({
  declarations: [
    ...PIPES,
    ...COMPONENTS,
    ...DIRECTIVES,
  ],
  imports: [
    CommonModule,
    ...THIRD_MODULES,
    ...NG_ZORRO_MODULES,
  ],
  exports: [
    ...THIRD_MODULES,
    ...NG_ZORRO_MODULES,
    ...PIPES,
    ...COMPONENTS,
    ...DIRECTIVES,
  ],
})
export class SharedModule {
}
`;

export const newAppModule = `import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { RouterStateSerializer, NgxsRouterPluginModule } from '@ngxs/router-plugin';
import { states } from './store';
import { CustomRouterStateSerializer } from './store';
import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { NgxsFormPluginModule } from '@ngxs/form-plugin';
import { NgxsModule } from '@ngxs/store';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    NgxsStoragePluginModule.forRoot(),
    NgxsFormPluginModule.forRoot(),
    NgxsRouterPluginModule.forRoot(),
    NgxsModule.forRoot([...states], { selectorOptions: {injectContainerState: false}, developmentMode: !environment.production,})
  ],
  providers: [
    {provide: RouterStateSerializer, useClass: CustomRouterStateSerializer}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
`;
