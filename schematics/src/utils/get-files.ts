import { Tree } from '@angular-devkit/schematics';

export function getFiles(_tree: Tree, dir: string) {
  if (!dir.endsWith('/')) {
    dir += '/';
  }
  let files: any[] = [];
  let folder = _tree.getDir(dir);
  let subFolder = folder.subdirs;
  if (subFolder && subFolder.length > 0) {
    subFolder.forEach(f => files = files.concat(getFiles(_tree, dir + f + '/')));
  }
  let subFiles = folder.subfiles;
  if (subFiles && subFiles.length > 0) {
    subFiles.forEach(f => files = files.concat(dir + f));
  }
  return files;
}
