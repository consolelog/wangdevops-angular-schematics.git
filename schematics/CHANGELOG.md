# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [3.0.1](https://gitee.com/consolelog/wangdevops-angular-schematics/compare/v3.0.0...v3.0.1) (2023-04-03)

## [3.0.0](https://gitee.com/consolelog/wangdevops-angular-schematics/compare/v1.3.1...v3.0.0) (2023-04-03)


### ⚠ BREAKING CHANGES

* 升级到angular15

### ✨ Features | 新功能

* 升级到angular15 ([2480ded](https://gitee.com/consolelog/wangdevops-angular-schematics/commit/2480ded42e226359ddded2b770dee0a2ca82e16f))
* 添加fix-state功能 ([64086d5](https://gitee.com/consolelog/wangdevops-angular-schematics/commit/64086d53ddac5b984887a6150c1c5f86aecc1c45))
* 添加依赖 ([973c9a0](https://gitee.com/consolelog/wangdevops-angular-schematics/commit/973c9a09a69f8d88b2719642f1c65f1357480bf4))

## [1.2.0](https://gitee.com/consolelog/wangdevops-angular-schematics/compare/v1.1.0...v1.2.0) (2022-08-25)


### ✨ Features | 新功能

* 添加@angular/cdk依赖 ([6960520](https://gitee.com/consolelog/wangdevops-angular-schematics/commit/69605201ea0dd8f6d2a37cd81794e3b122d7d1b1))

## [2.0.0](https://gitee.com/consolelog/wangdevops-angular-schematics/compare/v1.3.1...v2.0.0) (2023-03-27)


### ⚠ BREAKING CHANGES

* 升级到angular15

### ✨ Features | 新功能

* 升级到angular15 ([2480ded](https://gitee.com/consolelog/wangdevops-angular-schematics/commit/2480ded42e226359ddded2b770dee0a2ca82e16f))
* 添加依赖 ([973c9a0](https://gitee.com/consolelog/wangdevops-angular-schematics/commit/973c9a09a69f8d88b2719642f1c65f1357480bf4))

### [1.3.1](https://gitee.com/consolelog/wangdevops-angular-schematics/compare/v1.3.0...v1.3.1) (2022-09-29)


### ⏪ Reverts | 回退

* 修改state生成的路径 ([4171068](https://gitee.com/consolelog/wangdevops-angular-schematics/commit/41710680bee4530ce28d69bfbf3b40b501414162))

## [1.3.0](https://gitee.com/consolelog/wangdevops-angular-schematics/compare/v1.2.0...v1.3.0) (2022-09-15)


### ✨ Features | 新功能

* 添加格式化规范文件 ([386a0f4](https://gitee.com/consolelog/wangdevops-angular-schematics/commit/386a0f40978603f42c7a16955ecbe13f0c7c1243))

## [1.2.0](https://gitee.com/consolelog/wangdevops-angular-schematics/compare/v1.0.5...v1.2.0) (2022-09-15)


### 📝 Documentation | 文档

* 改用standard-version ([7cd7bc1](https://gitee.com/consolelog/wangdevops-angular-schematics/commit/7cd7bc186a20309bf0a7c0a0cca5517576108033))
* 更新markdown格式化规则 ([9432319](https://gitee.com/consolelog/wangdevops-angular-schematics/commit/9432319c6dd32d6bd788889fc3c64af4f342c896))


### ✨ Features | 新功能

* 添加object-path-immutable依赖 ([2e4343e](https://gitee.com/consolelog/wangdevops-angular-schematics/commit/2e4343ebf451be91b811d095e83994813eec6053))
* CHANGELOG生成工具改用standard-version ([e7f2eac](https://gitee.com/consolelog/wangdevops-angular-schematics/commit/e7f2eac2125bbad663b2a4ff1dc0bca9d765a474))

## [1.1.0](https://gitee.com/consolelog/wangdevops-angular-schematics/compare/v1.0.6...v1.1.0) (2022-08-25)


### ✨ Features | 新功能

* CHANGELOG生成工具改用standard-version ([e7f2eac](https://gitee.com/consolelog/wangdevops-angular-schematics/commit/e7f2eac2125bbad663b2a4ff1dc0bca9d765a474))

### [1.0.6](https://gitee.com/consolelog/wangdevops-angular-schematics/compare/v1.0.5...v1.0.6) (2022-08-25)


### 📝 Documentation | 文档

* 改用standard-version ([7cd7bc1](https://gitee.com/consolelog/wangdevops-angular-schematics/commit/7cd7bc186a20309bf0a7c0a0cca5517576108033))
* 更新markdown格式化规则 ([9432319](https://gitee.com/consolelog/wangdevops-angular-schematics/commit/9432319c6dd32d6bd788889fc3c64af4f342c896))

## [1.0.5](https://gitee.com/consolelog/wangdevops-angular-schematics/compare/v1.0.4...v1.0.5) (2022-08-12)

### ♻ Code Refactoring | 代码重构

* 删除无用代码 ([6bcafc2](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/6bcafc257d4185a0f8e921bdba3b06e54f7393b9))

### ✨ Features | 新功能

* 升级依赖版本 ([b44da2a](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/b44da2aa591bb9d8b53eddff8a2810879aec56db))

### 📝 Documentation | 文档

* 修改README.md ([dc5ca50](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/dc5ca502554cfb19d1f0328157aed93590a2ad8c))

## [1.0.4](https://gitee.com/consolelog/wangdevops-angular-schematics/compare/v1.0.3...v1.0.4) (2022-08-11)

### ✨ Features | 新功能

* 添加CHANGELOG ([d2629e1](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/d2629e19d81008a2c95ae0bb27bf9ed685fd1b35))
* 添加CHANGELOG生成规则 ([ad81660](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/ad816604c0286f8b1a183e1ae076f6bfe61f452d))
* 添加本地开发实用的代理配置 ([a0fdf9c](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/a0fdf9c04bb6acd4bce1083f4a13233f4658d36f))

### 📝 Documentation | 文档

* 修改README.md ([c0daef2](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/c0daef2130ae3bc8b9af1d94bf2d4341a1bf7451))

## [1.0.3](https://gitee.com/consolelog/wangdevops-angular-schematics/compare/v1.0.2...v1.0.3) (2022-08-11)

### ✨ Features | 新功能

* sharedModule添加import ([5098269](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/5098269d8c726435c13fd1829ae5f9cbc2551dc7))

## [1.0.2](https://gitee.com/consolelog/wangdevops-angular-schematics/compare/v1.0.1...v1.0.2) (2022-08-11)

### ✨ Features | 新功能

* 添加CHANGELOG生成工具 ([a3f3f22](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/a3f3f225bca07474d13074a971e979b84598069f))
* 添加MIT LICENSE. ([ddceddf](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/ddceddfbd3aef802241ba7eca71124df4f8df69c))

## [1.0.1](https://gitee.com/consolelog/wangdevops-angular-schematics/compare/v1.0.0...v1.0.1) (2022-08-11)

### ✨ Features | 新功能

* sharedModule中导入formPlugin ([af68802](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/af68802f04b963cc02c8d3c1525015daae02840b))

# [1.0.0](https://gitee.com/consolelog/wangdevops-angular-schematics/compare/v0.0.5...v1.0.0) (2022-08-11)

### 📝 Documentation | 文档

* 修改README.md ([a01cf78](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/a01cf785e51c6d30b888182432f7264960abf901))
* 修改README.md中图片的URL ([b6bed16](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/b6bed16b6909a79532d10e16d58d536980d6fc9a))
* 修改webstorm演示图片 ([74bb7ec](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/74bb7ece9be20fe7f47d054840300917fd06fb4a))

## [0.0.5](https://gitee.com/consolelog/wangdevops-angular-schematics/compare/v0.0.4...v0.0.5) (2022-08-11)

## [0.0.4](https://gitee.com/consolelog/wangdevops-angular-schematics/compare/v0.0.2...v0.0.4) (2022-08-11)

### ✨ Features | 新功能

* 修改state生成的路径 ([57875cb](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/57875cb247111f6cd635a2bc942cd5720d23bf1d))

## [0.0.2](https://gitee.com/consolelog/wangdevops-angular-schematics/compare/v0.0.1...v0.0.2) (2022-08-11)

### ✨ Features | 新功能

* 添加state生成逻辑 ([7710ce5](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/7710ce5b6011b1046616bad1dd71ea90781f334e))
* 添加对package.json的依赖进行排序的逻辑 ([728d2fa](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/728d2faa0854d33d4f1ba5cb9616f20099353c63))

### 👷‍ Build System | 构建

* 修改package.json中的keywords ([9a3c338](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/9a3c3389acda848e77f7b7598a2a53629677091e))

### 📝 Documentation | 文档

* 修改README.md ([cecdeb4](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/cecdeb46fa7ef170d9b6f818b9a55c7d4474fc71))

## [0.0.1](https://gitee.com/consolelog/wangdevops-angular-schematics/compare/v0.0.0...v0.0.1) (2022-08-11)

### ⏪ Reverts | 回退

* 还原tsconfig ([537e366](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/537e366079e040fafc45dbe7629923b0e719d849))

### ♻ Code Refactoring | 代码重构

* 优化代码 ([c4c64ce](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/c4c64ce5e3984bb27813038838767eeaa5ef75ff))
* 优化代码结构 ([cd24a80](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/cd24a809106615ca4044cbb5bd15649cc76315d5))
* 重构测试文件结构 ([5fd66c0](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/5fd66c09d49c4c6475f980cfd4c797dcdc68ee50))

### ✅ Tests | 测试

* 优化测试代码 ([44cdec7](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/44cdec7d8e698c86ea674eabaedb6ae1e0ad5833))
* 优化测试代码 ([63a9a9d](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/63a9a9d59e58c855b4353986aae2a03e4f5a154f))
* 优化测试方法 ([3ec8491](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/3ec8491fe7ee71800b862f531785fd48f2779d2f))
* 删除fit ([ee7876d](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/ee7876d35590154e8949fac74848e6f53c50e6b9))
* 删除fit ([0de130a](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/0de130ada7640cd4400945a8648c3b6f4a7e96cf))
* 完成添加配置文件逻辑 ([e794491](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/e7944912c49bb6d50fe44b73f955437654d2b2ba))
* 提取测试工具类 ([1a298ee](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/1a298ee2a6573306070dd9ba8a0c5d419905ad6d))
* 提取测试常量 ([558fa40](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/558fa409f7ef60a06012ddcc9ad1e09b42a0b4be))
* 提取测试常量 ([a7b04f7](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/a7b04f76493d273f0ab92785ffc1c56aa8bddd31))
* 提取测试所需的通用代码 ([9beab10](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/9beab1039984c642dff8ffd83c137df6226310f2))
* 测试工具中添加参数 ([c36767d](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/c36767d594894e40c2deafa6b7522a944df5b05a))
* 测试描述信息改为中文 ([03c40cf](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/03c40cf8279594b7f66e4540ea0d7cd90b9ef4dd))
* 添加package.json验证测试 ([4edf844](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/4edf8440e6ec25264c578c1fa4e2be432297667d))
* 添加测试方法 ([814dfe8](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/814dfe8a7fbd0f60dfa6d172866261ec72ee2eb1))

### ✨ Features | 新功能

* 修改ng-add逻辑 ([006af6a](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/006af6ab07e03692da90ad6b197fee91e9fefab7))
* 修改ng-add逻辑 ([300edb5](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/300edb5dea684c3622eb103bbe91621d65367f50))
* 修改ng-add逻辑 ([8d26699](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/8d26699a593236060337aeb5f01613cc40b80b66))
* 修改ng-add逻辑，sharedModule ([1a37c62](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/1a37c6255fa5920b1b65cff7bb30a70b71dc65a3))
* 更新依赖版本 ([ba38084](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/ba38084964906778ba947c46d9804273c4c74953))
* 添加dev依赖的安装 ([76fcfc8](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/76fcfc857caf007b300187795493e0af0cc43171))
* 添加ng-add状态管理相关逻辑 ([6609393](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/6609393f50389b4ed536d4a3f343f2fe771d389e))
* 添加provider ([1ec37ab](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/1ec37ab5b7bb0a5d535f96ae3c7f4077fac52112))
* 添加sharedModule ([5900e58](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/5900e58055dba0013b9f301e0b7067c3ae6916ce))
* 添加配置文件功能 ([79cbf27](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/79cbf27d6ad297438272d2f6689f56099b36184a))

### 👷‍ Build System | 构建

* 添加本地打包脚本 ([303ce16](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/303ce16971898f3e164c4f63e503477a13e9139f))

# [0.0.0](https://gitee.com/consolelog/wangdevops-angular-schematics/compare/c6f1ba67f145d3132cbb1af4f572264602a45196...v0.0.0) (2022-08-11)

### ✨ Features | 新功能

* 关闭analytics ([6cf3534](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/6cf3534151fc705724bc74b9ab400d740ec6a180))
* 添加ng-add模块 ([6c4d1e0](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/6c4d1e03c207fd493cb70dcae33dcb5760221a76))


* initial commit ([c6f1ba6](https://gitee.com/consolelog/wangdevops-angular-schematics/commits/c6f1ba67f145d3132cbb1af4f572264602a45196))
