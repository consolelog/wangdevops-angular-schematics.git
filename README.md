# 个人使用的 angular schematics

## 使用方法

### 安装依赖

```bash
ng add @wangdevops/angular-schematics
```

执行结束后会在 `$project/src/app` 目录下生成如下文件：

```
📒 $project //应用根目录
    📁 src/app
      📄 app.module.ts //appModule会自动添加import以及provider
      📁 shared //通用代码目录
        📄 shared.module.ts //通用代码模块
      📁 store //store存储目录
        📄 index.ts
        📁 router //路由状态管理
        📁 system //系统状态管理
    📄 changelog-options.js //CHANGELOG生成所用的规则配置
    📄 proxy.json //本地开发解决跨域使用的代理配置
    📄 package.json //node配置中会自动添加依赖
```

### 到对应目录生成状态管理模板文件

```bash
cd $project/src/app/pages/user # 这里是生成文件的位置
ng g @wangdevops/angular-schematics:state aaa # aaa是生成的store名称
```

这一步也可以在WebStorm的功能执行，操作如下：

![alt text](https://gitee.com/consolelog/wangdevops-angular-schematics/raw/master/1.png "WebStorm操作1")

找到对应的schematics

![alt text](https://gitee.com/consolelog/wangdevops-angular-schematics/raw/master/2.png "WebStorm操作1")

回车后输入状态名称"aaa"再按回车就可以生成了

## 源码地址

https://gitee.com/consolelog/wangdevops-angular-schematics.git

## 更新日志

https://gitee.com/consolelog/wangdevops-angular-schematics/blob/master/schematics/CHANGELOG.md
