import {
  apply,
  applyTemplates,
  MergeStrategy,
  mergeWith,
  move,
  Rule,
  SchematicContext,
  Tree,
  url,
} from '@angular-devkit/schematics';
import { InsertChange } from '@schematics/angular/utility/change';

import {
  addModuleImportToRootModule,
  addSymbolToNgModuleMetadata,
  hasNgModuleImport,
  insertImport,
  parseSourceFile,
} from '@angular/cdk/schematics';
import { resolveTree } from '../utils/testing';
import { UpdateRecorder } from '@angular-devkit/schematics/src/tree/interface';
import { normalize } from '@angular-devkit/core';

const imports: any[] = [
  {'RouterStateSerializer': '@ngxs/router-plugin'},
  {'states': './store'},
  {'CustomRouterStateSerializer': './store'},
  {'environment': '../environments/environment'},
];

export function addImport(): Rule {
  return async (_tree: Tree, _context: SchematicContext) => {
    let resolve = await resolveTree(_tree);
    const recorder = _tree.beginUpdate(resolve.appModulePath);
    const changes = [];
    for (const item of imports) {
      for (const itemKey in item) {
        let insertChange = insertImport(resolve.moduleSource, resolve.appModulePath, itemKey, item[itemKey]);
        changes.push(insertChange);
      }
    }
    changes.forEach((change) => {
      if (change instanceof InsertChange) {
        recorder.insertLeft(change.pos, change.toAdd);
      }
    });
    _tree.commitUpdate(recorder);
    return;
  };
}

const addModules: any[] = [
  {'BrowserModule': '@angular/platform-browser'},
  {'BrowserAnimationsModule': '@angular/platform-browser/animations'},
  {'SharedModule': './shared/shared.module'},
  {'NgxsStoragePluginModule.forRoot()': '@ngxs/storage-plugin'},
  {'NgxsFormPluginModule.forRoot()': '@ngxs/form-plugin'},
  {'NgxsRouterPluginModule.forRoot()': '@ngxs/router-plugin'},
  {'NgxsModule.forRoot([...states], { selectorOptions: {injectContainerState: false}, developmentMode: !environment.production,})': '@ngxs/store'},
];

export function addModule(): Rule {
  return async (_tree: Tree, _context: SchematicContext) => {
    let resolve = await resolveTree(_tree);
    for (const item of addModules) {
      for (const itemKey in item) {
        if (!hasNgModuleImport(_tree, resolve.appModulePath, itemKey)) {
          addModuleImportToRootModule(_tree, itemKey, item[itemKey], resolve.project);
        }
      }
    }
    return;
  };
}

const removeModules: any[] = [
  'ReactiveFormsModule',
  'FormsModule',
  'HttpClientModule',
];

export function removeModule(): Rule {
  return async (_tree: Tree, _context: SchematicContext) => {
    let resolve = await resolveTree(_tree);
    const recorder = _tree.beginUpdate(resolve.appModulePath);
    for (const item of removeModules) {
      removeModule1(_tree, resolve, recorder, item);
    }
    _tree.commitUpdate(recorder);
    return;
  };
}

function removeModule1(_tree: Tree, resolve: any, recorder: UpdateRecorder, module: string) {
  if (hasNgModuleImport(_tree, resolve.appModulePath, module)) {
    let text = parseSourceFile(_tree, resolve.appModulePath).text;
    const regex = `imports([\\S\\s]+)\\[([\\s\\S]*?)(?:\n?)(\\s*${module}(?:\\s*,?\n?))([\\s\\S]+?)\\]`;
    let result = new RegExp(regex).exec(text);
    if (!result) {
      return;
    }
    let index = result.index + result[1].length + result[2].length + 8;
    let length = result[3].length;
    recorder.remove(index, length);
  }
}

export function addProvider(): Rule {
  return async (_tree: Tree, _context: SchematicContext) => {
    let resolve = await resolveTree(_tree);
    const recorder = _tree.beginUpdate(resolve.appModulePath);

    const addProvide = addSymbolToNgModuleMetadata(resolve.moduleSource, resolve.appModulePath,
      'providers', `{provide: RouterStateSerializer, useClass: CustomRouterStateSerializer}`, null);
    const changes = [
      insertImport(resolve.moduleSource, resolve.appModulePath, 'RouterStateSerializer', '@ngxs/router-plugin'),
      insertImport(resolve.moduleSource, resolve.appModulePath, 'CustomRouterStateSerializer', './store'),
      ...addProvide,
    ];

    changes.forEach((change) => {
      if (change instanceof InsertChange) {
        recorder.insertLeft(change.pos, change.toAdd);
      }
    });
    _tree.commitUpdate(recorder);
    return;
  };
}

export function addFiles(_options: any): Rule {
  return async (_tree: Tree, _context: SchematicContext) => {
    let resolve = await resolveTree(_tree);
    return mergeWith(apply(url('./files'), [
      applyTemplates({}),
      move(normalize(`${resolve.project.root}`)),

    ]), MergeStrategy.AllowCreationConflict);
  };
}

export function addScript(): Rule {
  return async (_tree: Tree, _context: SchematicContext) => {
    const sourceText = _tree.read('/package.json')!.toString('utf-8');
    const json = JSON.parse(sourceText);
    json.scripts.release = 'standard-version';
    json.scripts.start = 'ng serve --proxy-config proxy.json';
    _tree.overwrite('/package.json', JSON.stringify(json, null, 2));
  };
}
