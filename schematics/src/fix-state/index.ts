import { chain, Rule, SchematicContext, Tree } from '@angular-devkit/schematics';
import { getFiles } from '../utils/get-files';
import { Project, SourceFile } from 'ts-morph';
import { camelize, classify } from '@angular-devkit/core/src/utils/strings';
import { resolveTree } from '../utils/testing';

export function fixState(_options: any): Rule {

  return (_tree: Tree, _context: SchematicContext) => {
    return chain([
      fixStateFiles(_options),
    ])(_tree, _context);
  };

}


export function fixStateFiles(_options: any): Rule {
  return async (_tree: Tree, _context: SchematicContext) => {
    let resolve = await resolveTree(_tree);
    //获取全部文件
    let files = getFiles(_tree, resolve.sourceRoot || '/');
    _context.logger.debug(`获取全部文件:\t${files}`);
    //过滤出全部action文件
    let actions = files.filter(s => s.endsWith('.action.ts'));
    _context.logger.debug(`过滤出全部action文件:\t${actions}`);
    let project = new Project({});
    for (let action of actions) {
      const regex = `(.*)/(.*).action.ts`;
      let result: any = new RegExp(regex).exec(action);
      //正则取到文件夹和文件名
      let folder = result[1];
      let name = result[2];
      _context.logger.debug(`正则取到文件夹和文件名:\t${folder}\t${name}`);

      //state文件内容
      let stateContent = _tree.readText(`${folder}/${name}.state.ts`);
      _context.logger.debug(`state文件内容:\t${stateContent}`);
      //state文件解析对象
      let stateSourceFile = project.createSourceFile(`${name}.state.ts`, stateContent);
      //action文件内容
      let actionContent = _tree.readText(`${folder}/${name}.action.ts`);
      _context.logger.debug(`action文件内容:\t${actionContent}`);
      //action文件解析对象
      let actionSourceFile = project.createSourceFile(`${name}.action.ts`, actionContent);
      _context.logger.debug(`action文件解析对象:done`);

      //新的state文件解析对象
      let newStateSourceFile = updateStateSourceFile(stateSourceFile, actionSourceFile, _context);
      _context.logger.debug(`新的state文件解析对象:done`);
      if (newStateSourceFile !== null) {
        //获取文件内容
        let newStateFileContent = newStateSourceFile.getFullText();
        _context.logger.debug(`获取文件内容:\t${newStateFileContent}`);
        //覆盖现有的state文件
        _tree.overwrite(`${folder}/${name}.state.ts`, newStateFileContent);
        _context.logger.debug(`覆盖现有的state文件:done`);
      }
    }
  };
}

function updateStateSourceFile(stateSourceFile: SourceFile, actionSourceFile: SourceFile, _context: SchematicContext) {
  //获取Action类（获取UserAction类）
  let action0 = (actionSourceFile.getStructure().statements as any[]).find(s => s.declarationKind === 'namespace');
  _context.logger.debug(`获取Action类:done`);
  //获取Action类名（UserAction）
  let actionName0 = action0.name;
  _context.logger.debug(`获取Action类名:\t${actionName0}`);
  //获取action中的全部action
  // 0 = "UserAction.ChangeId"
  // 1 = "UserAction.ChangeType"
  // 2 = "UserAction.DeleteType"
  let allAction = (action0.statements as any[]).map(s => `${actionName0}.${s.name}`);
  _context.logger.debug(`获取action中的全部action:\t${allAction}`);

  //获取state类名（UserState）
  let stateName = classify(camelize((stateSourceFile.getBaseNameWithoutExtension() as any).replaceAll('.', '-')));
  _context.logger.debug(`获取state类名:\t${stateName}`);
  //获取state类对象
  let stateClass = stateSourceFile.getClassOrThrow(stateName);
  _context.logger.debug(`获取state类对象:done`);
  //获取state中全部方法对象
  let stateMethods = stateClass.getMethods();
  _context.logger.debug(`获取state中全部方法对象:done`);
  //转换成字符串数组（[UserAction.ChangeId]）
  let allState = stateMethods.map(s => {
    return s.getDecorators().filter(j => j.getName() === 'Action').map(j => {
      return (j.getStructure().arguments as any)[0];
    })[0];
  });
  _context.logger.debug(`转换成字符串数组:\t${allState}`);
  //对比缺失的action
  // 0 = "UserAction.ChangeType"
  // 1 = "UserAction.DeleteType"
  let missingAction = allAction.filter(s => allState.indexOf(s) === -1);
  _context.logger.debug(`对比缺失的action:\t${missingAction}`);
  //向state中添加缺失的action
  if (missingAction && missingAction.length > 0) {
    for (let action of missingAction) {
      stateClass.addMethod({
        //ChangeType
        name: action.split('.')[1],
        //参数：ctx:StateContext<UserStateModel>
        parameters: [{name: 'ctx', type: `StateContext<${stateName}Model>`}],
        //注解：@Action(UserAction.ChangeType)
        decorators: [{name: 'Action', arguments: [action]}],
      });
    }
  } else {
    _context.logger.debug(`没有缺失的action, return null:done`);
    return null;
  }

  return stateSourceFile;
}
