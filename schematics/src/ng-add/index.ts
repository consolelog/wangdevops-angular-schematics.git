import { chain, Rule, SchematicContext, Tree } from '@angular-devkit/schematics';
import { addDependencies, addPackageInstallTask } from './dependencies';
import { addFiles, addImport, addModule, addProvider, addScript, removeModule } from './setup';

export function ngAdd(_options: any): Rule {
  return (_tree: Tree, _context: SchematicContext) => {
    return chain([
      addImport(),
      addModule(),
      removeModule(),
      addProvider(),
      addFiles(_options),
      addScript(),
      addDependencies(),
      addPackageInstallTask(),
    ])(_tree, _context);
  };
}
