import { SchematicTestRunner, UnitTestTree } from '@angular-devkit/schematics/testing';
import { createTestRunner, createTestTree } from '../utils/testing';


describe('fix-state', () => {

  let appTree: UnitTestTree;
  let testRunner: SchematicTestRunner;

  beforeEach(async () => {
    testRunner = createTestRunner();
    appTree = await createTestTree(testRunner);
  });

  const userActionContent = `export namespace UserAction {
  export class ChangeId {
    static readonly type = \`修改id\`;

    constructor(public id: number) {
    }
  }

  export class ChangeType {
    static readonly type = \`修改type\`;

    constructor(public type: string) {
    }
  }

  export class DeleteType {
    static readonly type = \`删除type\`;
  }
}
`;

  const orderActionContent = `export namespace OrderAction {
  export class ChangeId {
    static readonly type = \`修改id\`;

    constructor(public id: number) {
    }
  }

  export class ChangeType {
    static readonly type = \`修改type\`;

    constructor(public type: string) {
    }
  }

  export class DeleteType {
    static readonly type = \`删除type\`;
  }
}`;

  it('fix state', async () => {
    //添加user模块
    let options: any = {name: 'user', path: '/projects/bar/src/app/pages/user'};
    appTree = await testRunner.runSchematic('state', options, appTree);
    appTree.overwrite('/projects/bar/src/app/pages/user/user/user.action.ts', userActionContent);

    //添加order模块
    options = {name: 'order', path: '/projects/bar/src/app/pages/order'};
    appTree = await testRunner.runSchematic('state', options, appTree);
    appTree.overwrite('/projects/bar/src/app/pages/order/order/order.action.ts', orderActionContent);

    //执行fix
    let tree = await testRunner.runSchematic('fix', {}, appTree);
    //判断是否成功
    let userStateContent = tree.readContent(`/projects/bar/src/app/pages/user/user/user.state.ts`);
    expect(userStateContent).toContain(`@Action(UserAction.ChangeType)`);
    expect(userStateContent).toContain(`@Action(UserAction.DeleteType)`);
    let orderStateContent = tree.readContent(`/projects/bar/src/app/pages/order/order/order.state.ts`);
    expect(orderStateContent).toContain(`@Action(OrderAction.ChangeType)`);
    expect(orderStateContent).toContain(`@Action(OrderAction.DeleteType)`);
  });
  const bug001_file1 = `import { Data_Load, Data_LoadConfig } from '../constants';

export namespace DataAction {
  export class LoadConfig {
    static readonly type = Data_LoadConfig;
  }

  export class Load {
    static readonly type = Data_Load;
  }

}
`;
  const bug001_file2 = `import { Injectable } from '@angular/core';
import { Action, State, StateContext } from '@ngxs/store';
import { DataAction } from '.';
import { DataService } from '../../shared/service/data/data.service';
import { mergeMap, tap } from 'rxjs';

export interface DataStateModel {
  config: any[];
  data: any;
}

@State<DataStateModel>({
  name: 'data',
  defaults: {
    config: [],
    data: {},
  },
})
@Injectable({
  providedIn: 'root',
})
export class DataState {

  constructor(
      private service: DataService,
  ) {
  }

  @Action(DataAction.LoadConfig)
  LoadConfig(ctx: StateContext<DataStateModel>) {
    return this.service.loadConfig().pipe(tap((config: any) => {
      ctx.patchState({config});
    }), mergeMap(() => ctx.dispatch(new DataAction.Load())));
  }

  @Action(DataAction.Load)
  Load(ctx: StateContext<DataStateModel>) {
    let config = ctx.getState().config;
    if (!config || !Array.isArray(config) || config.length === 0) {
      return;
    }
    let count = 0;
    let result: any = {};
    for (let i = 0; i < config.length; i++) {
      const c = config[i];
      this.service.load(c.code).subscribe(r => {
        result[c.code] = r;
        count++;
        if (count === config.length) {
          ctx.patchState({data: result});
        }
      });
    }
  }


}
`;
  it('解决bug001', async () => {
    //添加data模块
    let options: any = {name: 'data', path: '/projects/bar/src/app/pages/data'};
    appTree = await testRunner.runSchematic('state', options, appTree);
    appTree.overwrite('/projects/bar/src/app/pages/data/data/data.action.ts', bug001_file1);
    appTree.overwrite('/projects/bar/src/app/pages/data/data/data.state.ts', bug001_file2);
    //执行fix
    let tree = await testRunner.runSchematic('fix', {}, appTree);
    //判断是否成功
    let userStateContent = tree.readContent(`/projects/bar/src/app/pages/data/data/data.state.ts`);

    expect(userStateContent.indexOf('@Action(DataAction.Load)') !== -1).toBe(true);
    userStateContent = userStateContent.replace('@Action(DataAction.Load)', '');
    expect(userStateContent.indexOf('@Action(DataAction.Load)') !== -1).toBe(false);
  });

});
