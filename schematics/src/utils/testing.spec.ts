import { createTestRunner, createTestTree, resolveTree } from './testing';
import { SchematicTestRunner, UnitTestTree } from '@angular-devkit/schematics/testing';

describe('testing', () => {
  let appTree: UnitTestTree;
  let testRunner: SchematicTestRunner;

  beforeEach(async () => {
    testRunner = createTestRunner();
    appTree = await createTestTree(testRunner);
  });

  it('resolve', async () => {
    let resolve = await resolveTree(appTree);
    expect(resolve.sourceRoot).toBe('projects/bar/src');
  });
});
